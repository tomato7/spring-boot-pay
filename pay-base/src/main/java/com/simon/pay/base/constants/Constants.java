package com.simon.pay.base.constants;

import org.springframework.util.ClassUtils;

public class Constants {

	//文件分隔符
	public static final String SF_FILE_SEPARATOR = System.getProperty("file.separator");
	//行分隔符
	public static final String SF_LINE_SEPARATOR = System.getProperty("line.separator");
	//路径分隔符
	public static final String SF_PATH_SEPARATOR = System.getProperty("path.separator");

	public static final String QRCODE_PATH = ClassUtils.getDefaultClassLoader().getResource("static").getPath()+SF_FILE_SEPARATOR+"qrcode"; 
	
	public static final String SUCCESS = "success";
	
	public static final String FAIL = "fail";

	/**
	 * 交易状态
	 */
	public static final String WAIT_BUYER_PAY = "WAIT_BUYER_PAY";
	public static final String TRADE_CLOSED = "TRADE_CLOSED";
	public static final String TRADE_SUCCESS = "TRADE_SUCCESS";
	public static final String TRADE_FINISHED = "TRADE_FINISHED";
}
