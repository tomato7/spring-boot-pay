package com.simon.pay.base.model;
import java.io.Serializable;
import lombok.Data;

/**
 * 产品订单信息
 */
@Data
public class Product implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 商品id
	 */
	private String productId;
	/**
	 * 订单名称
	 */
	private String subject;
	/**
	 * 商品描述
	 */
	private String body;
	/**
	 * 总金额（单位是分）
	 */
	private String totalFee;
	/**
	 * 唯一标识订单号
	 */
	private String outTradeNo;
	/**
	 * 发起人ip地址
	 */
	private String spbillCreateIp;
	/**
	 * 附件数据主要用于商户携带订单的自定义数据
	 */
	private String attach;
	/**
	 * 支付类型(1:支付宝 2:微信 3:银联)
	 */
	private Short payType;
	/**
	 * 支付方式（1.pc、平板 2:手机）
	 */
	private Short payWay;
	/**
	 * 前台回调地址，非扫码支付使用
	 */
	private String frontUrl;
	
	public Product() {
		super();
	}

	public Product(String productId, String subject, String body,
			String totalFee, String outTradeNo, String spbillCreateIp,
			String attach, Short payType, Short payWay, String frontUrl) {
		super();
		this.productId = productId;
		this.subject = subject;
		this.body = body;
		this.totalFee = totalFee;
		this.outTradeNo = outTradeNo;
		this.spbillCreateIp = spbillCreateIp;
		this.attach = attach;
		this.payType = payType;
		this.payWay = payWay;
		this.frontUrl = frontUrl;
	}
}
