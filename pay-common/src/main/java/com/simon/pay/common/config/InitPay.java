package com.simon.pay.common.config;

import com.alipay.demo.trade.config.Configs;
import com.simon.pay.common.util.union.SDKConfig;
import com.simon.pay.common.util.wechat.ConfigUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 启动加载支付宝、微信以及银联相关参数配置
 */
@Component
public class InitPay implements ApplicationRunner{
	@Override
    public void run(ApplicationArguments var){
		//初始化 支付宝-微信-银联相关参数,涉及机密,此文件不会提交,请自行配置相关参数并加载
		Configs.init("zfbinfo.properties");
		ConfigUtil.init("wxinfo.properties");
		SDKConfig.getConfig().loadPropertiesFromSrc();
    }
    
}