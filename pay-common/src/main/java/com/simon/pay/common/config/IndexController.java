package com.simon.pay.common.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 通用访问拦截匹配
 */
@Controller
public class IndexController {
	/**
	 * 页面跳转
	 */
	@RequestMapping("index.html")
	public String page() {
		return  "index";
	}
	/**
	 * 页面跳转(一级目录)
	 */
	@RequestMapping("{module}/{url}.html")
	public String page(@PathVariable("module") String module,@PathVariable("url") String url) {
		return module + "/" + url;
	}
	/**
	 * 页面跳转（二级目录)
	 */
	@RequestMapping("{module}/{sub}/{url}.html")
	public String page(@PathVariable("module") String module,@PathVariable("sub") String sub,@PathVariable("url") String url) {
		return module + "/" + sub + "/" + url;
	}
	
}
