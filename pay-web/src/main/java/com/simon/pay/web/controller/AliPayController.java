package com.simon.pay.web.controller;

import static com.simon.pay.base.constants.Constants.TRADE_CLOSED;
import static com.simon.pay.base.constants.Constants.TRADE_FINISHED;
import static com.simon.pay.base.constants.Constants.TRADE_SUCCESS;
import static com.simon.pay.base.constants.Constants.WAIT_BUYER_PAY;

import com.simon.pay.base.constants.Constants;
import com.simon.pay.base.model.Product;
import com.simon.pay.service.IAliPayService;
import com.simon.pay.common.util.ali.AliPayConfig;

import java.io.BufferedOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.demo.trade.config.Configs;

/**
 * 支付宝支付
 * @author John
 */
@Slf4j
@Controller
@RequestMapping(value = "alipay")
public class AliPayController {
	@Autowired
	private IAliPayService aliPayService;

	@PostMapping(value="pcPay")
    public String  pcPay(Product product,ModelMap map) {
		log.info("电脑支付");
		String form  =  aliPayService.aliPayPc(product);
		map.addAttribute("form", form);
		return "alipay/pay";
    }
	@PostMapping(value="mobilePay")
    public String  mobilePay(Product product,ModelMap map) {
		log.info("手机H5支付");
		String form  =  aliPayService.aliPayMobile(product);
		map.addAttribute("form", form);
		return "com/simon/pay/alipay/pay";
    }
	@PostMapping(value="qcPay")
    public String  qcPay(Product product,ModelMap map) {
		log.info("二维码支付");
		String message  =  aliPayService.aliPay(product);
		if(Constants.SUCCESS.equals(message)){
			String img= "../qrcode/"+product.getOutTradeNo()+".png";
			map.addAttribute("img", img);
		}else{
			//失败
		}
		return "alipay/qcpay";
    }
	@PostMapping(value="appPay")
    public String  appPay(Product product,ModelMap map) {
		log.info("app支付服务端");
		String orderString  =  aliPayService.appPay(product);
		map.addAttribute("orderString", orderString);
		return "com/simon/pay/alipay/pay";
    }
	
    /**
     * 支付宝支付后台回调(二维码、H5、网站)
     */
	@PostMapping(value="pay")
	public void alipayNotify(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String  message = "success";
		Map<String, String> params = new HashMap<>();
		// 取出所有参数是为了验证签名
		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String parameterName = parameterNames.nextElement();
			params.put(parameterName, request.getParameter(parameterName));
		}
		//验证签名 校验签名
		boolean signVerified = false;
		try {
			signVerified = AlipaySignature.rsaCheckV1(params, Configs.getAlipayPublicKey(), AliPayConfig.CHARSET, AliPayConfig.SIGN_TYPE);
			//各位同学这里可能需要注意一下,2018/01/26 以后新建应用只支持RSA2签名方式，目前已使用RSA签名方式的应用仍然可以正常调用接口，注意下自己生成密钥的签名算法
			//signVerified = AlipaySignature.rsaCheckV1(params, Configs.getAlipayPublicKey(), "UTF-8","RSA2");
			//有些同学通过 可能使用了这个API导致验签失败，特此说明一下
			//signVerified = AlipaySignature.rsaCheckV2(params, Configs.getAlipayPublicKey(), "UTF-8");//正式环境
		} catch (AlipayApiException e) {
			e.printStackTrace();
			message =  "failed";
		}
		if (signVerified) {
			log.info("支付宝验证签名成功！");
			// 若参数中的appid和填入的appid不相同，则为异常通知
			if (!Configs.getAppid().equals(params.get("app_id"))) {
				log.info("与付款时的appid不同，此为异常通知，应忽略！");
				message =  "failed";
			}else{
				String outTradeNo = params.get("out_trade_no");
				//在数据库中查找订单号对应的订单，并将其金额与数据库中的金额对比，若对不上，也为异常通知
				String status = params.get("trade_status");
				// 如果状态是正在等待用户付款
				if (WAIT_BUYER_PAY.equals(status)) {
					log.info(outTradeNo + "订单的状态正在等待用户付款");
				// 如果状态是未付款交易超时关闭，或支付完成后全额退款
				} else if (TRADE_CLOSED.equals(status)) {
					log.info(outTradeNo + "订单的状态已经关闭");
				// 如果状态是已经支付成功
				} else if (TRADE_SUCCESS.equals(status) || TRADE_FINISHED.equals(status)) {
					log.info("支付宝订单号:{}, 付款成功", outTradeNo);
					//这里 根据实际业务场景 做相应的操作
				} else {
					
				}
			}
		} else { // 如果验证签名没有通过
			message =  "failed";
			log.info("验证签名失败！");
		}
		BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream());
		out.write(message.getBytes());
		out.flush();
		out.close();
	}
	
	/**
	 * 支付宝支付PC端前台回调
	 */
	@PostMapping(value="/frontRcvResponse")
	public String  frontRcvResponse(HttpServletRequest request){
		try {
			//获取支付宝GET过来反馈信息
			Map<String,String> params = new HashMap<>();
			Map<String,String[]> requestParams = request.getParameterMap();
			for (String name : requestParams.keySet()) {
				String[] values = requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用
				valueStr = new String(valueStr.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
				params.put(name, valueStr);
			}
			//商户订单号
			String orderNo = new String(request.getParameter("out_trade_no").getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8);
			//前台回调验证签名 v1 or v2
			boolean signVerified = aliPayService.rsaCheckV1(params);
			if(signVerified) {
				log.info(":验证签名成功，订单号：{}", orderNo);
				//处理业务逻辑
			}else {
				log.info(":验证签名失败，订单号：{}", orderNo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			//处理异常信息
		}
		//支付成功、跳转到成功页面
		return "success.html";
	}
	
}
