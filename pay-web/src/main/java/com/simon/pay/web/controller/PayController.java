package com.simon.pay.web.controller;

import com.simon.pay.common.util.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 支付后台
 * @author John
 */
@Slf4j
@Controller
@RequestMapping(value = "pay")
public class PayController {
    @PostMapping(value="login")
    public @ResponseBody String login(String account,String password) {
        log.info("登陆");
        String param = "false";
        if("admin".equals(account)&&"111111".equals(password)){
            param =  "true";
        }
        return param;
    }

    @GetMapping(value="home")
    public String main(Model model) {
        model.addAttribute("ip", "192.168.1.66");
        model.addAttribute("address", "青岛");
        model.addAttribute("time", DateUtils.getTime());
        return "web/main";
    }
    
}
